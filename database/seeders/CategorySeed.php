<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(["name" => "Salado"]);
        Category::create(["name" => "Dulces"]);
        Category::create(["name" => "Bebidas"]);
        Category::create(["name" => "Desayuno"]);
        Category::create(["name" => "Almuerzo"]);
        Category::create(["name" => "Cena"]);

    }
}
