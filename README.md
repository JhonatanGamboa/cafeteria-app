# Cafeteria-app - Requisitos
- Version de Laravel 9
- PHP ^8.0
- Composer ^2.4.2

# Paso 1 - Creacion de base de datos
Crear base de datos 'cafeteria_db'

# Paso 2 - Configuracion de .env
Crear archivo .env, se puede crear en base al .env.example

# Paso 3 - Instalar dependencias
Ejercutar comando `composer install`

# Paso 4 - Crear app key
Ejercutar comando `php artisan key:generate`

# Paso 5 - Correr migraciones
Ejercutar comando `php artisan migrate --seed`

# Paso 6 - Arrancar servidor
Ejercutar comando `php artisan serve`

# Recomendaciones
Se recomienda dejar puerto por defecto 8000
