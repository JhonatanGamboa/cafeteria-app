<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SaleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('category', [CategoryController::class, 'index']);
Route::apiResource('product', ProductController::class);
Route::apiResource('sale', SaleController::class);
Route::get('product-stock', [ReportController::class, 'mostStockProductReport']);
Route::get('product-sale', [ReportController::class, 'mostSaleProduct']);

