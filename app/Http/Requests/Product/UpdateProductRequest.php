<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            "name" => [
                'string',
                Rule::unique('products', 'name')->ignore($this->product)
            ],
            "price" => 'numeric|min:1',
            "weight" => 'numeric|min:0',
            "stock" => 'numeric|min:0',
            "category_id" => 'integer|exists:categories,id'
        ];
    }
}
