<?php

namespace App\Http\Controllers;

use App\Http\Resources\Category\CategoryResource;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Return all the categories
     */
    public function index()
    {
        $categories = Category::all();
        return CategoryResource::collection($categories);
    }
}
