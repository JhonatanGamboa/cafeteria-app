<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Sale;

class ReportController extends Controller
{
    /**
     * This function return the product with most stock
     */
    public function mostStockProductReport()
    {
        $product = Product::selectRaw(
            "products.id AS product_id,
            products.name AS product_name,
            products.stock,
            categories.name AS category"
        )
            ->join("categories", "categories.id", "products.category_id")
            ->whereNull("products.deleted_at")
            ->orderBy("products.stock", "DESC")
            ->first();

        return response()->json(["data" => $product], 200);
    }

    /**
     * This function return the product with most sales
     */
    public function mostSaleProduct()
    {
        $product = Sale::selectRaw(
            "products.id AS product_id,
            products.name AS product_name,
            SUM( sales.amount ) AS total_amount,
            SUM( sales.total ) AS total_sales"
        )
            ->join("products", "products.id", "sales.product_id")
            ->whereNull("products.deleted_at")
            ->groupBy("sales.product_id")
            ->orderBy("total_amount", "DESC")
            ->first();

        return response()->json(["data" => $product], 200);
    }
}
