<?php

namespace App\Http\Controllers;

use App\Http\Requests\Sale\CreateSaleRequest;
use App\Http\Resources\Sale\SaleResource;
use App\Models\Product;
use App\Models\Sale;

class SaleController extends Controller
{
    /**
     * Return all the sales
     */
    public function index()
    {
        $sales = Sale::all();
        return SaleResource::collection($sales);
    }

    /**
     * Create a new sale
     */
    public function store(CreateSaleRequest $request)
    {
        $product = Product::find($request->product_id);

        // If the stock is lower than the amount solicited it will get an error message
        if($product->stock < $request->amount){
            return response()->json([
                "message" => "The current stock is lower than the amount solicited"
            ], 500);
        }

        // This will subtract the amount to the product stock
        $new_stock = $product->stock - $request->amount;
        $product->update(["stock" => $new_stock]);

        $request['total'] = $request->amount * $product->price;

        $sale = Sale::create($request->all());
        return new SaleResource($sale);
    }
}
