<?php

function makeReference($product_name){
    $characters = "0123456789$product_name";
    $randomstr = '';
    for ($i = 0; $i < 5; $i++) {
        $randomstr .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomstr;
}
